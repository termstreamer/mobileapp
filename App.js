import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Buffer } from 'buffer';

import Terminal from './components/Terminal';

import { throttle, ansiStrip, parseAsciCommand } from './common';

let contentInner = new Buffer(0);

const App = () => {
  const [ip, setIp] = useState('');
  const [port, setPort] = useState('');
  const [connected, setConnected] = useState(false);
  const [content, setContent] = useState('');
  const [terminalCommand, setTerminalCommand] = useState('');
  const [ws, setWS] = useState(undefined);

  const getTerminalText = (parsed) => {
    return parsed.cursorStyle.map(({ cursor, style }, index) => {
      let nextStyle = parsed.cursorStyle[index + 1] ? parsed.cursorStyle[index + 1] : {};
      return (
        <Text key={index} style={style}>{parsed.text.substr(cursor, (nextStyle.cursor || parsed.text.length) - cursor)}</Text>
      );
    });
  }

  const updateContent = throttle(() => {
    let text = ansiStrip(contentInner.toString());
    let parsed = {
      cursor: 0,
      text: '',
      cursorStyle: [{
        cursor: 0,
        style: {}
      }]
    };

    for (let i = 0; i < text.length; i++) {
      switch (text[i]) {
        case '\u0007': continue;
        case '\u00d0':
        case '\u00d1':
        case '\u0008': parsed.cursor--; continue;
        case '\u001b':
          let res = parseAsciCommand(text.substr(i), parsed);
          if (!res) continue;
          text = res.text;
          parsed = res.parsed;
          i = -1;
          continue;

        case '\n': parsed.cursor = parsed.text.length;
        default:
          let a = Array.from(parsed.text);
          a[parsed.cursor] = text[i];
          parsed.text = a.join('');
          parsed.cursor++;
      }
    }

    setContent(getTerminalText(parsed));
  }, 100);

  const connect = () => {
    const newWs = new WebSocket(`ws://${ip}:${port}`);

    newWs.onopen = () => {
      setConnected(true);
    };

    newWs.onmessage = e => {
      const data = Buffer.from(e.data);
      contentInner = Buffer.concat([contentInner, data]);
      contentInner = contentInner.slice(-5000);
      updateContent();
    };

    newWs.onerror = e => {
      console.log(e.message);
    };

    newWs.onclose = e => {
      setContent('');
      contentInner = new Buffer(0);
      setConnected(false);
    };

    setWS(newWs);
  }

  const sendCommand = () => {
    ws.send(terminalCommand + '\n');
    setTerminalCommand('');
  }

  // useEffect(() => {
  //   return ws && ws.close();
  // }, [ ws ])

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.container}>
          {!connected && (
            <View style={styles.config}>
              <Text style={styles.label}>IP</Text>
              <TextInput
                style={styles.input}
                value={ip}
                onChangeText={setIp}
              />
              <Text style={styles.label}>PORT</Text>
              <TextInput
                style={styles.input}
                value={port}
                onChangeText={setPort}
              />
              <TouchableOpacity
                style={styles.connectButton}
                onPress={connect}
              >
                <Text>CONNECT</Text>
              </TouchableOpacity>
            </View>
          )}
          {connected && (
            <View style={styles.terminalContainer}>
              <Terminal content={content} />
              <View style={styles.terminalInputContainer}>
                <TextInput
                  style={styles.terminalInput}
                  value={terminalCommand}
                  onChangeText={setTerminalCommand}
                  on
                />
                <TouchableOpacity
                  style={styles.terminalCommandButton}
                  onPress={sendCommand}
                />
              </View>
            </View>
          )}
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: '#282c34',
  },
  config: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: '#282c34',
  },
  label: {
    color: '#fff',
    fontWeight: 'bold',
  },
  input: {
    backgroundColor: '#fff',
    borderRadius: 10,
    width: 200,
    margin: 20,
    marginTop: 5,
    textAlign: 'center',
    padding: 10,
  },
  connectButton: {
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#bbeffd',
    borderRadius: 10,
    margin: 20,
  },
  terminalContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  terminalInputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 40,
    borderRadius: 20,
    width: Dimensions.get('window').width-30,
    backgroundColor: '#fff',
    marginVertical: 10,
  },
  terminalInput: {
    flex: 1,
    paddingLeft: 20,
  },
  terminalCommandButton: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#005bdb',
  }
});

export default App;
