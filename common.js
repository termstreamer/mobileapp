import { Buffer } from 'buffer';

export function throttle(func, ms) {

  let isThrottled = false,
    savedArgs,
    savedThis;

  function wrapper() {

    if (isThrottled) { // (2)
      savedArgs = arguments;
      savedThis = this;
      return;
    }

    func.apply(this, arguments); // (1)

    isThrottled = true;

    setTimeout(function () {
      isThrottled = false; // (3)
      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs);
        savedArgs = savedThis = null;
      }
    }, ms);
  }

  return wrapper;
}

const ColorsRGB = {
  black: [0, 0, 0],
  darkGray: [100, 100, 100],
  lightGray: [200, 200, 200],
  white: [255, 255, 255],

  red: [204, 0, 0],
  lightRed: [255, 51, 0],

  green: [0, 204, 0],
  lightGreen: [51, 204, 51],

  yellow: [204, 102, 0],
  lightYellow: [255, 153, 51],

  blue: [0, 0, 255],
  lightBlue: [26, 140, 255],

  magenta: [204, 0, 204],
  lightMagenta: [255, 0, 255],

  cyan: [0, 153, 255],
  lightCyan: [0, 204, 255],
};

const O = Object;

const TEXT = 0;
const BRACKET = 1;
const CODE = 2;

const colorCodes = ['black', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'lightGray', '', 'default'];
const colorCodesLight = ['darkGray', 'lightRed', 'lightGreen', 'lightYellow', 'lightBlue', 'lightMagenta', 'lightCyan', 'white', ''];
const styleCodes = ['', 'bright', 'dim', 'italic', 'underline', '', '', 'inverse'];
const asBright = {
  'red': 'lightRed',
  'green': 'lightGreen',
  'yellow': 'lightYellow',
  'blue': 'lightBlue',
  'magenta': 'lightMagenta',
  'cyan': 'lightCyan',
  'black': 'darkGray',
  'lightGray': 'white'
};
const types = {
  0: 'style',
  2: 'unstyle',
  3: 'color',
  9: 'colorLight',
  4: 'bgColor',
  10: 'bgColorLight'
};
const subtypes = {
  color: colorCodes,
  colorLight: colorCodesLight,
  bgColor: colorCodes,
  bgColorLight: colorCodesLight,
  style: styleCodes,
  unstyle: styleCodes
};


class Color {

  constructor(background, name, brightness) {
    this.background = background
    this.name = name
    this.brightness = brightness
  }

  get inverse() {
    return new Color(!this.background, this.name || (this.background ? 'black' : 'white'), this.brightness)
  }

  get clean() {
    return clean({
      name: this.name === 'default' ? '' : this.name,
      bright: this.brightness === Code.bright,
      dim: this.brightness === Code.dim
    })
  }

  defaultBrightness(value) {
    return new Color(this.background, this.name, this.brightness || value)
  }

  css(inverted) {
    const color = inverted ? this.inverse : this
    const rgbName = ((color.brightness === Code.bright) && asBright[color.name]) || color.name
    const prop = (color.background ? 'background:' : 'color:')
      , rgb = Colors.rgb[rgbName]
      , alpha = (this.brightness === Code.dim) ? 0.5 : 1

    return rgb
      ? (prop + 'rgba(' + [...rgb, alpha].join(',') + ');')
      : ((!color.background && (alpha < 1)) ? 'color:rgba(0,0,0,0.5);' : '') // Chrome does not support 'opacity' property...
  }
}

class Code {

  constructor(n) {
    if (n !== undefined) { this.value = Number(n) }
  }

  get type() {
    return types[Math.floor(this.value / 10)]
  }

  get subtype() {
    return subtypes[this.type][this.value % 10]
  }

  get str() {
    return (this.value ? ('\u001b\[' + this.value + 'm') : '')
  }

  static str(x) {
    return new Code(x).str
  }

  get isBrightness() {
    return (this.value === Code.noBrightness) || (this.value === Code.bright) || (this.value === Code.dim)
  }
}

O.assign(Code, {
  reset: 0,
  bright: 1,
  dim: 2,
  inverse: 7,
  noBrightness: 22,
  noItalic: 23,
  noUnderline: 24,
  noInverse: 27,
  noColor: 39,
  noBgColor: 49
})

export function rawParse(s) {

  let state = TEXT, buffer = '', text = '', code = '', codes = []
  let spans = []

  for (let i = 0, n = s.length; i < n; i++) {
    buffer += s[i]

    switch (state) {

      case TEXT:
        if (s[i] === '\u001b') { state = BRACKET; buffer = s[i]; }
        else { text += s[i] }
        break

      case BRACKET:
        if (s[i] === '[') { state = CODE; code = ''; codes = [] }
        else { state = TEXT; text += buffer }
        break

      case CODE:

        if ((s[i] >= '0') && (s[i] <= '9')) { code += s[i] }
        else if (s[i] === ';') { codes.push(new Code(code)); code = '' }
        else if ((s[i] === 'm') && code.length) {
          codes.push(new Code(code))
          for (const code of codes) { spans.push({ text, code }); text = '' }
          state = TEXT
        }
        else { state = TEXT; text += buffer }
    }
  }

  if (state !== TEXT) text += buffer

  if (text) spans.push({ text, code: new Code() })

  return spans
}


export function ansiStrip(text) {
  text = text.replace(/\u001b\u005d\u0030\u003b.+\u0007/g, '');
  //text = text.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-PRZcf-nqry=><]/g, '')
  return text;
}

export function hexToAsci(hex) {
  var str = '';
  hex = hex.replace(/1b5d303b.+071b5b30313b33326d/g, '');
  for (var n = 0; n < hex.length; n += 2) {
    str += String.fromCodePoint(parseInt(hex.substr(n, 2), 16));
  }
  return str;
}

// http://ascii-table.com/ansi-escape-sequences.php
const re = /^\u001b\[((?<simpleCommand>[su]|[0-9]{0,3}[JK])|(?<cursorCommand>[0-9]{0,3}[ABCDEFGH])|(?<modeCommand>=[0-9]{1,3}[lh])|(?<positionCommand>[0-9]{0,3};[0-9]{0,3}[Hf])|(?<styleCommand>([0-9]{1,3};)*[0-9]{0,3}m)|(?<keyboardCommand>([0-9]{1,3};[0-9]{1,3};)*p))/;
export function parseAsciCommand(text, parsed, styles) {
  const smallText = text.substr(0, 100);
  let test = /^\u001b\[.*H/.exec(smallText);
    if (test) console.log(Buffer.from(test[0]));
  const match = re.exec(smallText);
  //console.log(Buffer.from(match[0]), match[0].length, match.groups);
  if (!match || match.index !== 0) return false;

  if (match.groups.simpleCommand) return parseSimpleCommand(match, text, parsed);
  if (match.groups.cursorCommand) return parseCursorCommand(match, text, parsed);
  if (match.groups.modeCommand) return parseModeCommand(match, text, parsed);
  if (match.groups.positionCommand) return parsePositionCommand(match, text, parsed);
  if (match.groups.styleCommand) return parseStyleCommand(match, text, parsed, styles);
  if (match.groups.keyboardCommand) return parseKeyboardCommand(match, text, parsed);

  text = text.substr(match[0].length);
  return { text, parsed };
}

function parseSimpleCommand(match, text, parsed) {
  const cmd = match.groups.simpleCommand.substr(-1);
  switch (cmd) {
    case 's': parsed.savedCursor = parsed.cursor; break;
    case 'u': parsed.cursor = parsed.savedCursor === undefined ? parsed.cursor : parsed.savedCursor; break;
    case 'K': parsed.text = parsed.text.substr(0, parsed.cursor-1); break;
    case 'J': parsed.text = ''; parsed.cursor = 0; break;
  }
  parsed.cursorStyle = parsed.cursorStyle.filter(s => s.cursor <= parsed.cursor);
  text = text.substr(match[0].length);
  return { text, parsed };
}
function parseCursorCommand(match, text, parsed) {
  const dir = match.groups.cursorCommand.substr(-1);
  const value = parseInt(match.groups.cursorCommand);
  switch (dir) {
    case 'A': // UP
    case 'B': // DOWN
    case 'C': // FORWARD
    case 'D': // BACKWARD
      break;
  }
  console.log('CURSOR', dir, value);
  text = text.substr(match[0].length);
  return { text, parsed };
}
function parseModeCommand(match, text, parsed) {
  const mode = match.groups.modeCommand.substr(-1);
  const value = parseInt(match.groups.modeCommand.replace('=', ''));
  switch (mode) {
    case 'h': // ENABLE
    case 'l': // DISABLE
    break;
  }
  console.log('MODE', mode, value);
  text = text.substr(match[0].length);
  return { text, parsed };
}
function parsePositionCommand(match, text, parsed) {
  const mode = match.groups.positionCommand.substr(-1);
  const [ line, column ] = match.groups.positionCommand.split(';').map(v => parseInt(v));
  switch (mode) {
    case 'H':
    case 'f':
      break;
  }
  console.log('POSITION', mode, line, column);
  text = text.substr(match[0].length);
  return { text, parsed };
}
function parseKeyboardCommand(match, text, parsed) {
  const values = match.groups.keyboardCommand.replace('p', '').split(';').map(v => parseInt(v));
  const keyMap = {};
  for (let i = 0; i < values.length; i+=2) {
    keyMap[values[i]] = values[i+1];
  }
  console.log('KEYBOARD', keyMap);
  text = text.substr(match[0].length);
  return { text, parsed };
}
function parseStyleCommand(match, text, parsed, styles={}) {
  const values = match.groups.styleCommand.replace('m', '').split(';').map(v => parseInt(v));
  const styleCount = parsed.cursorStyle.length;
  let lastStyle = parsed.cursorStyle[styleCount-1];
  if (lastStyle && lastStyle.cursor !== parsed.cursor) {
    parsed.cursorStyle.push({
      cursor: parsed.cursor,
      style: lastStyle.style
    });
    lastStyle = parsed.cursorStyle[styleCount];
  }

  const getStyle = (attributes, styleName, userStyles) => {
    return userStyles[styleName] ? userStyles[styleName] : attributes;
  }

  values.map(value => {
    switch (value) {
      case 0: lastStyle.style = {}; break;
      case 1:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ fontWeight: 'bold' }, 'text-bold', styles)) };
        break;
      case 4:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ textDecorationLine: 'underline' }, 'text-underscore', styles)) };
        break;
      case 5:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({}, 'text-blink', styles)) };
        break;
      case 7:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({}, 'text-negative', styles)) };
        break;
      case 8:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: 'transparent' }, 'text-concealed', styles)) };
        break;

      case 30:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: 'black' }, 'color-black', styles)) };
        break;
      case 31:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: 'red' }, 'color-red', styles)) };
        break;
      case 32:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: 'green' }, 'color-green', styles)) };
        break;
      case 33:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: 'yellow' }, 'color-yellow', styles)) };
        break;
      case 34:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: 'blue' }, 'color-blue', styles)) };
        break;
      case 35:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: '#ff00ff' }, 'color-magenta', styles)) };
        break;
      case 36:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: 'cyan' }, 'color-cyan', styles)) };
        break;
      case 37:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: 'white' }, 'color-white', styles)) };
        break;
      case 39:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ color: undefined }, 'color-default', styles)) };
        break;

      case 40:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: 'black' }, 'background-black', styles)) };
        break;
      case 41:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: 'red' }, 'background-red', styles)) };
        break;
      case 42:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: 'green' }, 'background-green', styles)) };
        break;
      case 43:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: 'yellow' }, 'background-yellow', styles)) };
        break;
      case 44:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: 'blue' }, 'background-blue', styles)) };
        break;
      case 45:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: '#ff00ff' }, 'background-magenta', styles)) };
        break;
      case 46:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: 'cyan' }, 'background-cyan', styles)) };
        break;
      case 47:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: 'white' }, 'background-white', styles)) };
        break;
      case 49:
        lastStyle.style = { ...lastStyle.style, ...(getStyle({ backgroundColor: undefined }, 'background-default', styles)) };
        break;

    }
  })
  text = text.substr(match[0].length);
  return { text, parsed };
}